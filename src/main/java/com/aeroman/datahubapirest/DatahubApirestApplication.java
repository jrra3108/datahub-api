package com.aeroman.datahubapirest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DatahubApirestApplication {

    public static void main(String[] args) {
        SpringApplication.run(DatahubApirestApplication.class, args);
    }

}
