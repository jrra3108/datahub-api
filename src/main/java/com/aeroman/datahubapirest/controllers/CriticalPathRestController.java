package com.aeroman.datahubapirest.controllers;

import com.aeroman.datahubapirest.models.entity.CriticalPaths;
import com.aeroman.datahubapirest.models.services.ICriticalPathService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/api")
public class CriticalPathRestController {

    @Autowired
    private ICriticalPathService criticalPathService;

    @GetMapping("/critical_paths")
    public List<CriticalPaths> index() {
        return criticalPathService.findAll();
    }

    @GetMapping("/critical_paths/{id}")
    public CriticalPaths show(@PathVariable UUID id){
        return criticalPathService.findById(id);
    }

    @PostMapping("/critical_paths")
    @ResponseStatus(HttpStatus.CREATED)
    public CriticalPaths create(@RequestBody CriticalPaths criticalPaths) {
        criticalPaths.setCreatedBy("system");
        criticalPaths.setUpdatedBy("system");
        return criticalPathService.save( criticalPaths);
    }

    @PutMapping("/critical_paths/{id}")
    public CriticalPaths update(@RequestBody CriticalPaths criticalPaths, @PathVariable UUID id) {
        CriticalPaths criticalPathsActual = criticalPathService.findById(id);

        criticalPathsActual.setAircraft(criticalPaths.getAircraft());
        criticalPathsActual.setWorkOrder(criticalPaths.getWorkOrder());
        criticalPathsActual.setCriticalPath(criticalPaths.getCriticalPath());
        criticalPathsActual.setTatImpact(criticalPaths.getTatImpact());
        criticalPathsActual.setMitigationPlan(criticalPaths.getMitigationPlan());
        criticalPathsActual.setResponsible(criticalPaths.getResponsible());
        criticalPathsActual.setStatus(criticalPaths.getStatus());
        criticalPathsActual.setStatusFinal(criticalPaths.getStatusFinal());

        return criticalPathService.save(criticalPathsActual);
    }

    @DeleteMapping("/critical_paths/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable UUID id){
        criticalPathService.delete(id);
    }
}
