package com.aeroman.datahubapirest.controllers;

import com.aeroman.datahubapirest.models.entity.DailyFocus;
import com.aeroman.datahubapirest.models.services.IDailyFocusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/api")
public class DailyFocusRestController {

    @Autowired
    private IDailyFocusService dailyFocusService;

    @GetMapping("/daily_focus")
    public List<DailyFocus> index() { return dailyFocusService.findAll(); }

    @GetMapping("/daily_focus/{id}")
    public DailyFocus show(@PathVariable UUID id){
        return dailyFocusService.findById(id);
    }

    @PostMapping("/daily_focus")
    @ResponseStatus(HttpStatus.CREATED)
    public DailyFocus create(@RequestBody DailyFocus dailyFocus) {
        dailyFocus.setCreatedBy("system");
        dailyFocus.setUpdatedBy("system");

        return dailyFocusService.save(dailyFocus);
    }

    @PutMapping("/daily_focus/{id}")
    public DailyFocus update(@RequestBody DailyFocus dailyFocus, @PathVariable UUID id) {
        DailyFocus dailyFocusActual = dailyFocusService.findById(id);

        dailyFocusActual.setAircraft(dailyFocus.getAircraft());
        dailyFocusActual.setWorkOrder(dailyFocus.getWorkOrder());
        dailyFocusActual.setTask(dailyFocus.getTask());
        dailyFocusActual.setComplete(dailyFocus.getComplete());
        dailyFocusActual.setReason(dailyFocus.getReason());
        dailyFocusActual.setDateId(dailyFocus.getDateId());

        return dailyFocusService.save(dailyFocusActual);
    }

    @DeleteMapping("/daily_focus/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable UUID id) { dailyFocusService.delete(id); }

}
