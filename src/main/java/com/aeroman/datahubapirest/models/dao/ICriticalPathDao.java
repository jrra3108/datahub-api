package com.aeroman.datahubapirest.models.dao;

import com.aeroman.datahubapirest.models.entity.CriticalPaths;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface ICriticalPathDao extends CrudRepository<CriticalPaths, UUID> {
}
