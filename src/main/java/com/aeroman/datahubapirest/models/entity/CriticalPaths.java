package com.aeroman.datahubapirest.models.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Entity
@Table(name = "CRITICAL_PATHS")
public class CriticalPaths implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;

    @Column(name = "AIRCRAFT")
    private String aircraft;

    @Column(name = "WORK_ORDER")
    private String workOrder;

    @Column(name = "CRITICAL_PATH")
    private String criticalPath;

    @Column(name = "TAT_IMPACT")
    private String tatImpact;

    @Column(name = "MITIGATION_PLAN")
    private String mitigationPlan;

    @Column(name = "RESPONSIBLE")
    private String responsible;

    @Column(name = "STATUS")
    private char status;

    @Column(name = "STATUS_FINAL")
    private char statusFinal;

    @Column(name = "CREATED_AT")
    @Temporal(TemporalType.DATE)
    private Date createdAt;

    @Column(name = "CREATED_BY")
    private String createdBy;

    @Column(name = "UPDATED_AT")
    @Temporal(TemporalType.DATE)
    private Date updatedAt;

    @Column(name = "UPDATED_BY")
    private String updatedBy;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getAircraft() {
        return aircraft;
    }

    public void setAircraft(String aircraft) {
        this.aircraft = aircraft;
    }

    public String getWorkOrder() {
        return workOrder;
    }

    public void setWorkOrder(String workOrder) {
        this.workOrder = workOrder;
    }

    public String getCriticalPath() {
        return criticalPath;
    }

    public void setCriticalPath(String criticalPath) {
        this.criticalPath = criticalPath;
    }

    public String getTatImpact() {
        return tatImpact;
    }

    public void setTatImpact(String tatImpact) {
        this.tatImpact = tatImpact;
    }

    public String getMitigationPlan() {
        return mitigationPlan;
    }

    public void setMitigationPlan(String mitigationPlan) {
        this.mitigationPlan = mitigationPlan;
    }

    public String getResponsible() {
        return responsible;
    }

    public void setResponsible(String responsible) {
        this.responsible = responsible;
    }

    public char getStatus() {
        return status;
    }

    public void setStatus(char status) {
        this.status = status;
    }

    public char getStatusFinal() {
        return statusFinal;
    }

    public void setStatusFinal(char statusfinal) {
        this.statusFinal = statusfinal;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    private static final long serialVersionUID = 1L;

    @PrePersist
    public void prePersist() {
        createdAt = new Date();
        updatedAt = new Date();
    }

    @PreUpdate
    public void preUpdate() {
        updatedAt = new Date();
    }
}
