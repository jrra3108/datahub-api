package com.aeroman.datahubapirest.models.services;

import com.aeroman.datahubapirest.models.entity.CriticalPaths;
import java.util.List;
import java.util.UUID;

public interface ICriticalPathService {

    public List<CriticalPaths> findAll();

    public CriticalPaths findById(UUID id);

    public CriticalPaths save(CriticalPaths criticalPaths);

    public void delete(UUID id);
}
