package com.aeroman.datahubapirest.models.servicesImp;

import com.aeroman.datahubapirest.models.dao.ICriticalPathDao;
import com.aeroman.datahubapirest.models.entity.CriticalPaths;
import com.aeroman.datahubapirest.models.services.ICriticalPathService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.UUID;

@Service
public class CriticalPathServiceImpl implements ICriticalPathService {

    @Autowired
    private ICriticalPathDao criticalPathDao;

    @Override
    @Transactional(readOnly = true)
    public List<CriticalPaths> findAll() {
        return (List<CriticalPaths>) criticalPathDao.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public CriticalPaths findById(UUID id) {
        return criticalPathDao.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public CriticalPaths save(CriticalPaths criticalPaths) {
        return criticalPathDao.save(criticalPaths);
    }

    @Override
    @Transactional
    public void delete(UUID id) {
        criticalPathDao.deleteById(id);
    }
}
